from ananke.graphs import ADMG
from ananke.identification import OneLineID
from ananke.estimation import CausalEffect

import numpy as np
import pandas as pd


def default_data_args():
    return dict(
        m_nonlinear=False,
        randomize=False,
        n=1000,

        c_var=0.02,
        a_dim=6,
        m_mean=100,
        m_var=1,

        #        C     D    A
        y_mean=[-200, -200, 4],
        y_int=200,
        y_var=1,
    )


def sample_data(**kwargs):
    """
    Sample a dataset from the given arguments.
    """

    np.random.seed(42)

    args = default_data_args()
    args.update(kwargs)

    m_nonlinear = args["m_nonlinear"]
    randomize = args["randomize"]
    n = args["n"]
    a_dim = args["a_dim"]

    c = np.random.normal(0, args["c_var"], size=[n, 1])
    d = np.random.randint(0, 2, n)

    if randomize:
        a = np.random.randint(0, a_dim, n)
    else:
        plusminus = np.power(-1, (np.arange(a_dim) < a_dim // 2).astype(int))
        thresholds = np.random.uniform(0, 1, size=[n, 2, 1])
        probs = np.ones([1, a_dim]) / a_dim + c * plusminus
        probs = np.stack([probs for _ in range(2)], axis=1)

        a = np.argmax(thresholds < np.cumsum(probs, axis=2), axis=2)
        a *= np.stack([np.ones(n, dtype=int), d], axis=1)
        a = np.max(a, axis=1)

    m_mean = args["m_mean"]
    m_var = args["m_var"]
    if m_nonlinear:
        b = (a + 1) / (1 + a_dim)
        m = np.random.normal(loc=m_mean * (1.05 * b - np.power(b, 2)),
                             scale=m_var, size=n)
    else:
        m = np.random.normal(loc=m_mean * (a + 1) / 7, scale=m_var, size=n)

    c = c.reshape(n)
    y_mean = np.array(args["y_mean"])
    y = np.random.normal(np.dot(y_mean, (c, d, m)), args["y_var"])
    y += args["y_int"]

    df = pd.DataFrame(data=dict(c=c, d=d, a=a, m=m, y=y))

    return df


def estimate_backdoor(df, method):
    vertices = ['a', 'c', 'd', 'm', 'y']
    di_edges = [('a', 'm'), ('m', 'y'), ('c', 'a'), ('d', 'a'), ('c', 'y'), ('d', 'y')]
    bi_edges = []
    G = ADMG(vertices, di_edges, bi_edges)
    one_id = OneLineID(graph=G, treatments=['a'], outcomes=['y'])
    one_id.id()
    ace_obj = CausalEffect(graph=G, treatment='a', outcome='y', quiet=True)

    a_dim = np.unique(df["a"]).shape[0]
    estimates = ace_obj.compute_counterfactuals(df, method)
    return np.array([estimates[i] for i in range(a_dim)])


def estimate_frontdoor(df, method):
    vertices = ['a', 'c', 'd', 'm', 'y']
    di_edges = [('a', 'm'), ('m', 'y')]
    bi_edges = [('a', 'y')]
    G = ADMG(vertices, di_edges, bi_edges)
    one_id = OneLineID(graph=G, treatments=['a'], outcomes=['y'])
    one_id.id()
    ace_obj = CausalEffect(graph=G, treatment='a', outcome='y', quiet=True)

    a_dim = np.unique(df["a"]).shape[0]
    estimates = ace_obj.compute_counterfactuals(df, method)
    return np.array([estimates[i] for i in range(a_dim)])


def naive_estimator(df):
    results = []
    for i in sorted(np.unique(df["a"])):
        est = np.mean(df[df["a"] == i]["y"])
        results.append(est)
    return np.array(results)


def test():
    m_nonlinear = False

    a_dim = 2
    df_2r = sample_data(n=10000, a_dim=a_dim, m_nonlinear=m_nonlinear, randomize=True)
    ground_truth = naive_estimator(df_2r)

    df_2 = sample_data(n=10000, a_dim=a_dim, m_nonlinear=m_nonlinear)

    print("{:9s} {}".format("truth", " ".join([f"{x:5.1f}" for x in ground_truth])))
    for method in ["gformula", "ipw", "aipw", "eff-aipw"]:
        backdoor = estimate_backdoor(df_2.copy(), method)
        print("{:9s} {}".format(method, " ".join([f"{x:5.1f}" for x in backdoor])))

    for method in ["p-ipw", "d-ipw", "apipw", "eff-apipw"]:
        frontdoor = estimate_frontdoor(df_2, method)
        print("{:9s} {}".format(method, " ".join([f"{x:5.1f}" for x in frontdoor])))

    df_6r = sample_data(n=10000, a_dim=6, m_nonlinear=m_nonlinear, randomize=True)
    ground_truth = naive_estimator(df_6r)

    df_6 = sample_data(n=10000, a_dim=6, m_nonlinear=m_nonlinear)

    print("{:9s} {}".format("truth", " ".join([f"{x:5.1f}" for x in ground_truth])))
    for method in ["gformula", "ipw", "aipw",]:
        backdoor = estimate_backdoor(df_6.copy(), method)
        print("{:9s} {}".format(method, " ".join([f"{x:5.1f}" for x in backdoor])))

    for method in ["p-ipw", "d-ipw", "apipw", "eff-apipw"]:
        frontdoor = estimate_frontdoor(df_6, method)
        print("{:9s} {}".format(method, " ".join([f"{x:5.1f}" for x in frontdoor])))


truth     154.0 214.1
gformula  155.5 212.6
ipw       155.6 212.6
aipw      155.5 212.6
eff-aipw  155.6 212.6
p-ipw     165.6 206.6
d-ipw      69.4 122.0
apipw      69.4 122.0
eff-apipw  69.4 122.0

truth     155.4 214.9 268.6 327.0 382.9 441.9
gformula  155.6 212.7 269.8 326.9 384.0 441.1
ipw         inf  33.5  48.2  64.3  85.0 105.7
aipw        nan 205.4 262.5 319.6 376.7 433.9
p-ipw      -9.3  10.3  14.9  19.7  26.0  31.9
d-ipw      20.7  30.5  43.8  58.5  77.3  96.1
apipw      20.7  30.5  43.8  58.5  77.3  96.1
eff-apipw  20.7  30.5  43.8  58.5  77.3  96.1


if __name__ == "__main__":
    test()
